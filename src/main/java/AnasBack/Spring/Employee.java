package AnasBack.Spring;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Setter
@Entity
@Table(name="employees")
public class Employee {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id ;

    @Column(name="first_name")
    private String firstName ;

    @Column(name="last_name")
    private String lastName ;

    @Column(name="email")
    private String email ;

    @Column(name="password")
    private String password ;


}
