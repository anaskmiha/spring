package AnasBack.Spring;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




@RestController
@RequestMapping("/api/products")
public class EmployeeController {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
    @Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping
    public List<Employee> getAllEmployees() {
        logger.info("Récupération de tous les employés");
        return employeeRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Employee> getEmployeeById(@PathVariable Integer id) {
        logger.info("Récupération de  un employée");
        return employeeRepository.findById(id);
    }

    @PostMapping
    public Employee createProduct(@RequestBody Employee employee) {
        logger.info("création d'un employée");
        return employeeRepository.save(employee);
    }


    @PutMapping("/updatedEmployee/{id}")
    public Employee updateEmployee( @PathVariable Integer id,  Employee updatedEmployee) {
        if (id == null) {
            throw new IllegalArgumentException("L'ID ne peut pas être nul."+ id);
        }
        Optional<Employee> employee = employeeRepository.findById(id);
        if (employee.isPresent()) {
            Employee existingEmployee = employee.get();
            existingEmployee.setFirstName(updatedEmployee.getFirstName());
            existingEmployee.setLastName(updatedEmployee.getLastName());
            existingEmployee.setEmail(updatedEmployee.getEmail());
            existingEmployee.setPassword(updatedEmployee.getPassword());
            return employeeRepository.save(existingEmployee);
        } else {
            return null;
        }
    }


    @DeleteMapping("/{id}")
    public void deleteEmployee(@PathVariable Integer id) {
        logger.info("supprimer un employée");
        employeeRepository.deleteById(id);
    }
}
